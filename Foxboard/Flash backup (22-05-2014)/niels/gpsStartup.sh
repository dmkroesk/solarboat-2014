#!/usr/bin/shell

echo "Setting up /dev/ttyS3 to baud rate 4800"
stty -F /dev/ttyS3 4800

echo "Done setting up /dev/ttyS3 to baud rate 4800"

echo "Now lets start the GPS / GPRS Telit module."
./toggleTelitOnOff
echo "The Telit GPS / GPRS module is toggled in activity. (It is most likely on now)"

