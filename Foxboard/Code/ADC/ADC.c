#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"    
#include "linux/gpio_syscalls.h"

#include "ADC.h"

int main(int argc, char **argv)
{
	/* Channel 0 */
	initializeMCP3008();
	writeStartBit(channelZero);
	double valueChannel0 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 1 */
	initializeMCP3008();
	writeStartBit(channelOne);
	double valueChannel1 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 2 */
	initializeMCP3008();
	writeStartBit(channelTwo);
	double valueChannel2 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 3 */
	initializeMCP3008();
	writeStartBit(channelThree);
	double valueChannel3 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 4 */
	initializeMCP3008();
	writeStartBit(channelFour);
	double valueChannel4 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 5 */
	initializeMCP3008();
	writeStartBit(channelFive);
	double valueChannel5 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 6 */
	initializeMCP3008();
	writeStartBit(channelSix);
	double valueChannel6 = (((double)readDigitalValue()) * 5) / 1024;
	
	/* Channel 7 */
	initializeMCP3008();
	writeStartBit(channelSeven);
	double valueChannel7 = (((double)readDigitalValue()) * 5) / 1024;
	
	printf("CH0:%f;CH1:%f;CH2:%f;CH3:%f;CH4:%f;CH5:%f;CH6:%f;CH7:%f;\n", valueChannel0, valueChannel1, valueChannel2, valueChannel3, valueChannel4, valueChannel5, valueChannel6, valueChannel7);
	
	return 1;
}

/* Set direction of LX832 ports for use with the MCP3008 */
void setLX832(void)
{
	gpiosetdir(PORTG, DIRIN, digitalOutPin);		
	gpiosetdir(PORTG, DIROUT, PG8_15);
	
	gpioclearbits(PORTG, digitalInPin);
	gpioclearbits(PORTG, clockPin);
	gpioclearbits(PORTG, selectShutdownPin);
}

/* Initialize MCP3008 (write falling edge) */
void initializeMCP3008(void)
{
	gpiosetbits(PORTG, selectShutdownPin); 				//SET SELECT SHUTDOWNPIN HIGH
	gpioclearbits(PORTG, clockPin); 					//SET CLOCKPIN LOW
	gpioclearbits(PORTG, selectShutdownPin); 			//SET SELECT SHUTDOWNPIN LOW
}

/* Write start bit to MCP3008, specifying the channel to read from */
void writeStartBit(int channel)
{
	int commandOut = channel; 							//CHANNEL
	commandOut |= 0x18;									//Start bit + single-ended bit
	commandOut <<= 3;									//5 bits

	int i;
	
	for(i = 0; i < 5; i++)
	{
		if(commandOut & 0x80)
		{
			gpiosetbits(PORTG, digitalInPin);
		}
		else
		{
			gpioclearbits(PORTG, digitalInPin);
		}

		commandOut <<= 1;

		writeClockTick();
	}
}

/* Write clock tick to MCP3008 */
void writeClockTick(void)
{
	gpiosetbits(PORTG, clockPin);						//SET CLOCKPIN HIGH
	gpioclearbits(PORTG, clockPin);						//SET CLOCKPIN LOW
}

/* Read digital value from MCP3008 */
int readDigitalValue()
{
	int result = 0;
	
	int i = 0;
	//Read in one empty bit, one null bit and 10 ADC bits
	for (i = 0; i < 12; i++) 
	{
		writeClockTick();
		
		result <<= 1;
		
		int bit = (gpiogetbits(PORTG, digitalOutPin)) ? (1):(0);
		
		if(bit == 1)
		{
			result |= 0x1;
		}
	} 
	
	gpiosetbits(PORTG, PG19); 							//SLEEP MCO3008

	result >>= 1;  										//Removing first bit since it is NULL
	
	return result;
}
