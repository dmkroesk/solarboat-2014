#ifndef ADC_INC
#define ADC_INC

#define digitalOutPin PG0							//		digital out
#define digitalInPin PG13							//		digital in
#define selectShutdownPin PG14						// 		select/shut-down
#define clockPin PG15								// 		clock

#define channelZero 0
#define channelOne 1
#define channelTwo 2
#define channelThree 3
#define channelFour 4
#define channelFive 5
#define channelSix 6
#define channelSeven 7

//METHOD DECLERATIONS
void setLX832(void);
void initializeMCP3008(void);
void writeStartBit(int channel);
void writeClockTick(void);
int readDigitalValue();

#endif
