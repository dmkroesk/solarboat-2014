// GPS reader, decodes GPRMC en GPGGA strings
//
// Diederich Kroeske, 2014
//
#ifndef GPSREADER_INC
#define GPSREADER_INC

// Defines
#define GPSBUFSIZE	200

#include "timer.h"
char gpsJson[1024];


// User types
typedef struct
{
	int h, m, s;
}TIME_TIME_STRUCT;

typedef struct
{
	int d, m, y;	
}TIME_DATE_STRUCT;

typedef struct
{
	TIME_TIME_STRUCT utc;
	TIME_DATE_STRUCT date;
	unsigned char status;
	char latitude[10];
	char longitude[10];
	char NS;
	char EW;
	char gs[10];
	char course[10];
	char mode;

}GPRMC_STRUCT;

typedef struct
{
	TIME_TIME_STRUCT utc;
	char latitude[20];
	char longitude[20];
	char NS;
	char EW;
	unsigned char pfi;
	unsigned char satellitesUsed;
	char hdop[10];
	char mslAltitude[10];
}GPGGA_STRUCT;

// 
void handleGpsThread();
int gpsCapture(GPRMC_STRUCT *gprmc);
char* getGps();

#endif

