#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include "gpsreader.h"


// Globals (Storage for the nmea sequences)
char gpsbuf[GPSBUFSIZE];
char decbuf[GPSBUFSIZE];
unsigned char bufi = 0;
unsigned char pstate = 0;

// Local function prototypes
int openSerialPort(char *descriptor);
uint8_t handleGPS(char rxchar,GPRMC_STRUCT *gprmc ); //, GPGGA_STRUCT *gpgga);
uint8_t decodeGPRMC(char *buf, GPRMC_STRUCT *gprmc);
uint8_t decodeGPGGA(char *buf, GPGGA_STRUCT *gpgga);
void *gpsHandler(void *ptr);
uint8_t char2int(char c);
uint16_t hex2int(char *asc);
void showGPRMC( GPRMC_STRUCT *gprmc);


//thread vars:
uint8_t retval;

// Try to capture valid GPS ()
// TO DO: timeout
GPRMC_STRUCT gprmc;
	
void handleGpsThread()
{
	while(1==1)
	{
		printf("Starting with the gps capture.\r\n");
		// uint8_t retval;

		// // Try to capture valid GPS ()
		// // TO DO: timeout
		// GPRMC_STRUCT gprmc;
		retval = gpsCapture(&gprmc);
		
		//Get the system time.
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		printf("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
		
		gprmc.date.y = tm.tm_year + 1900;
		gprmc.date.m = tm.tm_mon + 1;
		gprmc.date.d = tm.tm_mday;
		gprmc.utc.h = tm.tm_hour;
		gprmc.utc.m = tm.tm_min;
		gprmc.utc.s = tm.tm_sec;
		
		// Construct JSON
		if( retval )
		{
			//Set the JSON body of the message. The header will be set in restclient.c	
			// sprintf(gpsJson, "={\"longitude\": \"%s\",		\
							// \"latitude\": \"%s\",			\
							// \"groundspeed\": \"%s\",		\
							// \"course\": \"%s\",				\
							// \"northsouth\": \"%c\",			\
							// \"eastwest\": \"%c\",			\
							// \"foxtimestamp\": \"%02i-%02i-%02iT%02i:%02i:%02i.000\", ", gprmc.longitude, gprmc.latitude, gprmc.gs, gprmc.course, 'a', 'b', gprmc.date.y, gprmc.date.m, gprmc.date.d, gprmc.utc.h, gprmc.utc.m, gprmc.utc.s);
			
			
			//time_t t = time(NULL);
			//struct tm tm = *localtime(&t);
			//
			//printf("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
			
			
			sprintf(gpsJson, "={\"longitude\": \"%s\",		\
							\"latitude\": \"%s\",			\
							\"groundspeed\": \"%s\",		\
							\"course\": \"%s\",				\
							\"northsouth\": \"%c\",			\
							\"eastwest\": \"%c\",			\
							\"foxtimestamp\": \"%02i-%02i-%02iT%02i:%02i:%02i.000\", ", gprmc.longitude, gprmc.latitude, gprmc.gs, gprmc.course, 'a', 'b', gprmc.date.y, gprmc.date.m, gprmc.date.d, gprmc.utc.h, gprmc.utc.m, gprmc.utc.s);
		}
		else
		{
			//Set the JSON body of the message. The header will be set in restclient.c	
			sprintf(gpsJson, "={\"\": \"\", ");
		}
		
		printf("JSON: %s\r\n\r\n", gpsJson);
		
		usleep(5000000);
	}
}

char* getGps()
{
	//Get the system time.
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	printf("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	
	gprmc.date.y = tm.tm_year + 1900;
	gprmc.date.m = tm.tm_mon + 1;
	gprmc.date.d = tm.tm_mday;
	gprmc.utc.h = tm.tm_hour;
	gprmc.utc.m = tm.tm_min;
	gprmc.utc.s = tm.tm_sec;
	
	//if( retval )
	//{
		//Set the JSON body of the message. The header will be set in restclient.c	
		sprintf(gpsJson, "={\"longitude\": \"%s\",		\
						\"latitude\": \"%s\",			\
						\"groundspeed\": \"%s\",		\
						\"course\": \"%s\",				\
						\"northsouth\": \"%c\",			\
						\"eastwest\": \"%c\",			\
						\"foxtimestamp\": \"%02i-%02i-%02iT%02i:%02i:%02i.000\", ", gprmc.longitude, gprmc.latitude, gprmc.gs, gprmc.course, 'a', 'b', gprmc.date.y, gprmc.date.m, gprmc.date.d, gprmc.utc.h, gprmc.utc.m, gprmc.utc.s);
	//}
	//else
	//{
		//Set the JSON body of the message. The header will be set in restclient.c	
		//sprintf(gpsJson, "={\"\": \"\", ");
	//}
	//2014-06-05T20:50:03.937
	return gpsJson;
}

int gpsCapture(GPRMC_STRUCT *gprmc)
{
	// initialise serial port
	int gps = openSerialPort("/dev/ttyS3");

	// Baudrate enz ....
	struct termios options;
	memset (&options, 0, sizeof(struct termios));
	tcgetattr (gps, &options);	

	cfsetospeed (&options, (speed_t)B4800);
	cfsetispeed (&options, (speed_t)B4800);

	options.c_cflag &= ~PARENB;        // Make 8n1
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

	options.c_cflag &= ~CRTSCTS;       // no flow control
	options.c_cflag |= CREAD | CLOCAL; // turn on READ & ignore ctrl lines

	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	/* set serieel in raw mode (no processing op binnenkomende data) */
	cfmakeraw(&options);
	tcsetattr ( gps, TCSANOW, &options );

	printf("Probeer GPS RMC te decoderen ...\n");
	char ch;
	uint8_t done = 0;
	
	unsigned long timeOut = getMillisecondsTime() + 30000;
	
	while(!done)
	{
		if( read(gps, &ch, 1) > 0 )
		{
			// Try to decode valid 
			done = handleGPS(ch, gprmc);
		}
				
		if(timeOut < getMillisecondsTime())
		{
			printf("It took to long.\r\n");
			return done;
		}
		
	}

	printf("Gelukt: geldig RMC ontvangen ...\n");
	
	close(gps);

	return done;
}

int openSerialPort(char *descriptor)
{
	int gps = open( descriptor, O_RDWR | O_NOCTTY | O_NDELAY );

	if ( gps == -1 )
	{
		printf("openSerialPort: unable to open %s\n", descriptor);
		exit(-1);
	}	

	fcntl(gps, F_SETFL, 0);

	return gps;
}

uint8_t handleGPS(char rxchar, GPRMC_STRUCT *gprmc) //, GPGGA_STRUCT *gpgga)
{
	uint8_t retval = 0;

	switch(pstate)
	{
		// Loop for '$'. Start new nmea sequence
		case 0:
			if( rxchar == '$' )
			{
				bufi = 0;
				pstate = 1;
			}
			break;
		
		// Store rx chars in buf till 'lf' is received.
		case 1: // Check for CR
			if( rxchar == 0x0D )
			{
				pstate = 2;
			}
			else
			{
				if( bufi < GPSBUFSIZE )
				{
					gpsbuf[bufi++] = rxchar;
				}
				else
				{
					pstate = 0;
				}
			}
			break;
			
		case 2: // Check for LF
			if( rxchar == 0x0A )
			{
				// Add '\0' to satisfy string manipulation functions
				gpsbuf[bufi] = '\0';
				
				// copy buffer (decoder kills buffer!)
				strcpy(decbuf, gpsbuf);
				
				// Try to decode RMC strings else GGA's
				retval = decodeGPRMC(decbuf, gprmc);
				if( retval )
				{
					showGPRMC(gprmc);

				}
				// if( !retval )
				// {
				// 	retval = decodeGPGGA(gpsbuf, gpgga);
				// }
			}
			pstate = 0;
			break;
		
		default:
			pstate = 0;
			break;
	}
	return retval;
}


uint8_t decodeGPRMC(char *buf, GPRMC_STRUCT *gprmc)
{
	uint8_t done = 0;				// bail out flag
	char *token;					// 
	uint8_t state = 0;  			// internal state
	uint8_t retval = 0; 			// Assume fault
	uint16_t checksum = 0;
	uint16_t calculatedChecksum = 0;
	uint8_t count = 0;
		
	// First, calculate checksum
	while( buf[count] != '*' && count < 200 )
	{
		calculatedChecksum ^= buf[count];
		count++;
	}
	
	// Second, parse GPRMC string
	state = 0;
	while( !done  )
	{
		/*
		 * Do not use strtok, strtok skips empty fields !!
		 */
		token = strsep(&buf,",*");
		
		/* 
		 * convert into struct
		 */
		switch( state )
		{
			case 0:
				if( 0 != strcmp("GPRMC",token) )
				{
					done = 1;
				}
				break;
			
			case 1: // UTC time
				gprmc->utc.h = (token[0]-'0')*10+token[1]-'0';
				gprmc->utc.m = (token[2]-'0')*10+token[3]-'0';
				gprmc->utc.s = (token[4]-'0')*10+token[5]-'0';
				break;
			
			case 2: // Status
				if( 'A' == token[0] )
				{
					gprmc->status = 1;
				}
				else
				{
					gprmc->status = 0;
					done = 1;
				}
				break;				

			case 3: // Latitude
				strcpy(gprmc->latitude, token);
				break;

			case 4: // N/S indicator
				gprmc->NS = token[0];
				break;

			case 5: // Longitude
				strcpy(gprmc->longitude, token);
				break;
			
			case 6: // E/W indicator
				gprmc->EW = token[0];
				break;
			
			case 7: // groundspeed [knots]
				strcpy(gprmc->gs, token);
				break;
			
			case 8: // track [degree] / course
				strcpy(gprmc->course, token);
				break;
			
			case 9: // date
				gprmc->date.d = (token[0]-'0')*10+token[1]-'0';
				gprmc->date.m = (token[2]-'0')*10+token[3]-'0';
				gprmc->date.y = (token[4]-'0')*10+token[5]-'0';
				break;
			
			case 10: // Magnetic Variation Degrees. skip
				break;
			
			case 11: // Magnetic Variation E/W);. skip
				break;
			
			case 12: // Mode
				gprmc->mode = token[0];						
				break;
			
			case 13: // Checksum
				checksum = hex2int(token);
				if( checksum == calculatedChecksum )
				{
					retval = 1;
				}
				break;
			
			default:
				done = 1;
				break;
		}
		state++;
	}
	return retval;
}

uint8_t decodeGPGGA(char *buf, GPGGA_STRUCT *gpgga)
{
	uint8_t done = 0;				// bail out flag
	char *token;					// 
	uint8_t state = 0;  			// internal state
	uint8_t retval = 0; 			// Assume fault
	uint16_t checksum = 0;
	uint16_t calculatedChecksum = 0;
	uint8_t count = 0;
	
	// First, calculate checksum
	while( buf[count] != '*' && count < 200 )
	{
		calculatedChecksum ^= buf[count];
		count++;
	}
	
	// Second, parse GPGGA string
	state = 0;
	while( !done  )
	{
		/*
		 * Do not use strtok, strtok skips empty fields !!
		 */
		token = strsep(&buf,",*");
		
		/* 
		 * convert into struct
		 */
		switch( state )
		{
			case 0: // GGA protocol header
				if( 0 != strcmp("GPGGA",token) )
				{
					done = 1;
				}
				break;
			
			case 1: // UTC time
				gpgga->utc.h = (token[0]-'0')*10+token[1]-'0';
				gpgga->utc.m = (token[2]-'0')*10+token[3]-'0';
				gpgga->utc.s = (token[4]-'0')*10+token[5]-'0';
				break;			

			case 2: // Latitude
				strcpy(gpgga->latitude, token);
				break;

			case 3: // N/S indicator
				gpgga->NS = token[0];
				break;

			case 4: // Longitude
				strcpy(gpgga->longitude, token);
				break;
			
			case 5: // E/W indicator
				gpgga->EW = token[0];
				break;
			
			case 6: // Position Fix Indicator
				gpgga->pfi = token[0];
				break;
			
			case 7: // Satellites Used
				gpgga->satellitesUsed = token[0];
				break;

			case 8: // HDOP, Horizontal Dilution of Precision
				strcpy(gpgga->hdop,token);
				break;

			case 9: // MSL Altitude (mean sea level, calculated)
				strcpy(gpgga->mslAltitude, token);
				break;

			case 10: // units of antenne altitude
				break;

			case 11: // Geoidal Separation ?? skip
				break;

			case 12: // Units of geoidal seperation
				break;

			case 13: // Age of Diff. Corr. ?? Skip
				break;
			
			case 14: // This field is not in de mediatec datasheet !
				break;
			
			case 15: // Checksum
				checksum = hex2int(token);
				if( checksum == calculatedChecksum )
				{
					//tx('g');
					retval = 1;
				}
				break;
			
			default:
				done = 1;
				break;
		}
		state++;
	}

	return retval;
}


uint16_t hex2int(char *asc)
{		
	return( (char2int(asc[0]) << 4) + char2int(asc[1]));
}

uint8_t char2int(char c)
{
	uint8_t retval = 0;

	if( c >= '0' && c <= '9')
	{
		retval = c - '0'; 
	}
	else
	{
		retval = toupper(c) + 10 - 'A';
	}
	
	return retval;
}

void showGPRMC( GPRMC_STRUCT *gprmc)
{
	printf("--------------  GPRMC_STRUCT *gprmc ------------------\n");
	printf("utc:	%d:%d:%d\n", gprmc->utc.h, gprmc->utc.m, gprmc->utc.s);
	printf("date:	%d:%d:%d\n", gprmc->date.d, gprmc->date.m, gprmc->date.y);
	printf("status:	%d\n", gprmc->status);
	printf("lat:	%s\n", gprmc->latitude);
	printf("lon:	%s\n", gprmc->longitude);
	printf("gs:		%s\n", gprmc->gs);
	printf("course:	%s\n", gprmc->course);
	printf("mode	%c\n\n", gprmc->mode);
}
	
	
	

