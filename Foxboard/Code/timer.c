//This timer is for collecting and sending the data.
//This is both the data from the FOX Board and the info from the Mobyboxx

#include "timer.h"


unsigned long getMillisecondsTime()
{
	struct timeval tv;       

	if(gettimeofday(&tv, NULL) != 0) 
	{
		return 0;
	}

	return (unsigned long)((tv.tv_sec * 1000ul) + (tv.tv_usec / 1000ul));
}

void timerFunc()
{
	unsigned long milliseconds, restMilliseconds = 0;

	milliseconds = getMillisecondsTime();
	restMilliseconds = getMillisecondsTime();

	printf("Current milliseconds = %lu\n", milliseconds);
	
	//pointer for the gps json
	char *json;
	//the combined string of the gps json and the mobiiboxx json.
	char combined[9216];
	
	int gpsTimeout = 7500;
	
	while(1)
	{
		//Every 5 seconds retrieve the gps values and put them in the struct.
		
		if(milliseconds + gpsTimeout <= getMillisecondsTime())
		{
			printf("%i second or more have passed.\n", (gpsTimeout / 1000));

			printf("Current milliseconds = %lu\n", milliseconds);
			
			//Set the gps values in the struct
			//json = doGps();
			json = getGps();
			
			char *pntr = &solarboatInfo.buffer[1];
			
			//printf("JSON STEFAN: %s", pntr);
			
			
			
			sprintf(combined, "%s%s", json, pntr);

			milliseconds = getMillisecondsTime();
		}
		
		//send the json to thomas
		if(restMilliseconds + 15000 <= getMillisecondsTime())
		{
			printf("\r\n\r\nCOMBINED WE GET: \r\n%s\r\n\r\n", combined);
			RESTcall("avansti.nl", combined, "");
			
			restMilliseconds = getMillisecondsTime();
		}
		
		
	}
	
}



