#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <time.h>
#include <resolv.h>
#include <netinet/tcp.h>

#include "restclient.h"

/* 
 * Restclient voor het versturen van data naar restserver
 */
void error(const char *msg)
{
    perror(msg);
    abort();
}

/*****************************************************************************/
int RESTcall(char *url, char *pload, char *feedid)
//int main(int argc, char *argv[])
/*
 * Description  : Update xively with payload
 *
 * input        :
 * output       : 
 */
/*****************************************************************************/
{
    
    int sockfd = 0;
    struct sockaddr_in serv_addr;
    struct hostent *server;

   
    // // Lees json file in buffer
    // char pload[4096];
    // FILE *json_fp = fopen(argv[1],"r");
    // if( NULL == json_fp )
    // {
    //     error("Kan json niet openen");
    // }

    // unsigned index = 0;
    // do pload[index++] = fgetc(json_fp); 
    //     while( 0 == feof(json_fp) );
    // pload[--index] = '\0';
    // fclose(json_fp);  
  
    // Create TCP socket en doe host lookup
    if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    {
        error("ERROR opening socket");
    }
    int i = 1; 
    setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (void *)&i, sizeof(i));
    
    if( (server = gethostbyname(url)) == NULL ) 
    {
        error("ERROR, no such host\n");
    }
    
    // Set connection struct and connect port 80
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(80);
    bcopy((char *)server->h_addr, 
        (char *)&serv_addr.sin_addr.s_addr,
        server->h_length);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,
        sizeof(serv_addr)) < 0) 
    {
        error("ERROR connecting");
    }

    // Construct http header
    char header[4096];
    unsigned long contentLenght = strlen(pload);
    //sprintf(header, "PUT /v2/feeds/%s HTTP/1.1\r\n"         \
    //                "Host: api.xively.com\r\n"              \
    //                "X-PachubeApiKey: %s\r\n"               \
    //                "Content-Length: %lu\r\n"               \
    //                "Content-Type: application/json\r\n"    \
    //                "Connection: close\r\n\r\n",            \
    //                feedid,                                 \
    //                XIVELY_API_KEY,                         \
    //                contentLenght                           \
    //                );
	
	
	
    sprintf(header, "POST /api/solar/ HTTP/1.1\r\nHost: avansti.nl\r\nContent-Length: %lu\r\nContent-Type: application/x-www-form-urlencoded\r\nConnection: Close\r\n\r\n", contentLenght);
	
    printf("#-------------------MESSAGE-------------------#:\r\n %s%s\r\n#--------------------------------------#", header, pload);
	
	
	
    send(sockfd, header, strlen(header), 0);
    send(sockfd, pload, strlen(pload), 0);
 
    char buf[4096];
    int bytesRead;
    do
    {
        bzero(buf, sizeof(buf));
        bytesRead = recv(sockfd, buf, sizeof(buf), 0);
        printf("#-------------------RESPONSE-------------------#\r\n %s\r\n\r\n", buf);
    } while( bytesRead > 0 );
   
    // socket close
    close(sockfd);
        
    return 0;
}
