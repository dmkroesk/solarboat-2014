#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

#include "sbdl.h"

#include <pthread.h>

//prototype

unsigned long getMillisecondsTime();

void timerFunc();


#endif

