// 
// Avans Solarboat data logger
//
// Runs op foxboard LX832
//


#include "sbdl.h"

//char gpsJson[1024];

int main()
{
	//Globallay used return code for thread creation
	int returnCode;
	
	//Assign a thread for the timer.
	pthread_t timerThread;
	// Solarboat server thread
	pthread_t solarboatThread;
	// GPS thread
	pthread_t gpsThread;

	// init mutex
	pthread_mutex_init(&mutex, NULL);
	// init gemeenschappelijke data van solarboat thread
	strcpy(solarboatInfo.buffer, ".\"\": \"\"}"); // lege json string. Dit zorgt ervoor dat ook zonder stefan zijn link er gewoon data gestuurd kan worden.
	solarboatInfo.msgNumber = 0;

	//Create a thread for the timer.
	returnCode = pthread_create(&timerThread, NULL, timerFunc, NULL);
	
	if(returnCode)
	{
		printf("ERROR; return code from pthread_create() for the timer thread is %d\n", returnCode);
		exit(-1);
	}
	
	// Create solarboat server thread;
	returnCode = pthread_create(&solarboatThread, NULL, handleSolarBoatServer, NULL);
	
	if(returnCode)
	{
		printf("ERROR; return code from pthread_create() for the solarboat server is %d\n", returnCode);
		exit(-1);
	}
	
	//Create a thread for the timer.
	returnCode = pthread_create(&gpsThread, NULL, handleGpsThread, NULL);
	
	if(returnCode)
	{
		printf("ERROR; return code from pthread_create() for the GPS thread is %d\n", returnCode);
		exit(-1);
	}
	
	
	// Run forever
	while(1 == 1)
	{
		//print the solarboat info.
		printf("Solarboat says (msgNr=%.4X): %s", solarboatInfo.msgNumber, solarboatInfo.buffer);	
		usleep(5000000);
	}

	// Hier kom je nooit (embedded sw runs forever) maar voor de volledigheid ...
	pthread_join(timerThread, NULL);
	pthread_join(solarboatThread, NULL);
	pthread_join(gpsThread, NULL);

	//Shutdown all threads.
	pthread_exit(NULL);

	return 0;
}

char* doGps()
{
	uint8_t retval;

	// Try to capture valid GPS ()
	// TO DO: timeout
	GPRMC_STRUCT gprmc;
	retval = gpsCapture(&gprmc);
	
	// Construct JSON
	if( retval )
	{
		//Set the JSON body of the message. The header will be set in restclient.c	
		sprintf(gpsJson, "={\"longitude\": \"%s\",		\
						\"latitude\": \"%s\",			\
						\"groundspeed\": \"%s\",		\
						\"course\": \"%s\",				\
						\"northsouth\": \"%c\",			\
						\"eastwest\": \"%c\",			\
						\"foxtimestamp\": \"%02i-%02i-%02iT%02i:%02i:%02i.000\", ", gprmc.longitude, gprmc.latitude, gprmc.gs, gprmc.course, 'a', 'b', gprmc.date.y, gprmc.date.m, gprmc.date.d, gprmc.utc.h, gprmc.utc.m, gprmc.utc.s);
	}
	else
	{
		//Set the JSON body of the message. The header will be set in restclient.c	
		sprintf(gpsJson, "={\"\": \"\", ");
	}
	//2014-06-05T20:50:03.937
	return gpsJson;
}
