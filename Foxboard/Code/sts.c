#include "sts.h"

//
// Elke incoming connection
//
void *handleConnection(void *psocket)
{
	int done = 0;

	// Start receiving data
	int socket = *(int *)psocket;

	char recvBuffer[RECVBUFFERSIZE];
	int numberOfBytesRead;
	while( !done )
	{
		if( (numberOfBytesRead = recv(socket, recvBuffer, sizeof(recvBuffer), 0)) > 0 )
		{
			recvBuffer[numberOfBytesRead] = '\0';
			pthread_mutex_lock(&mutex);
			strcpy(solarboatInfo.buffer,recvBuffer);
			solarboatInfo.msgNumber++;
			pthread_mutex_unlock(&mutex);
		}
		else
		{
			done = 1; // socket wordt geclosed, of error
		}
	}
	close(*(int*)psocket);
	return 0;
}	

//
void *handleSolarBoatServer(void *arg)
{
	pthread_t thread;
	int nrSolarboatInfoPackets = 0;

	// Setup a server which accepts connection
	int serverSocket = 0; 
	struct sockaddr_in serv_addr; 

	// Create socket
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERVER_PORT); 

	// Set timeout, wil je dit?
	struct timeval tv;
	tv.tv_sec = TIMEOUTSECONDS;
	tv.tv_usec = 0;
	setsockopt(serverSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));

	// Bind the serverSocket
	bind(serverSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

	// Start to listen on the serverSocket
	listen(serverSocket, 10);

	// Server never stops ...
	int runerr = 0;

	int clientSocket;
	struct sockaddr_in client_addr;
	socklen_t socketsize = sizeof(struct sockaddr_in);

	// Run forever
	while( (clientSocket = accept(serverSocket, (struct sockaddr*)&client_addr, &socketsize)) )
	{ 
		nrSolarboatInfoPackets++;
		printf("Solarboat connected from %s \n", inet_ntoa(client_addr.sin_addr) );

		if( pthread_create(&thread, NULL, handleConnection, (void *)&clientSocket) < 0 )
		{
			fprintf(stderr, "Failed to create thread\n");
			runerr = 1;
		} 
	};

	fprintf(stderr, "Solarboat server will end. This is bad!!\n");

	// Close the connection with the client
	close(serverSocket);   

	return 0;
} 

