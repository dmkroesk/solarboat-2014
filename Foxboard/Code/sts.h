#ifndef STS_H
#define STS_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <pthread.h>

#define SERVER_PORT     	17801
#define TIMEOUTSECONDS		300
#define RECVBUFFERSIZE   	8192

// Gemeenshappelijke buffer def. In deze buffer staat de JSON die we van de E studenten
// krijgen. De buffer wordt beschermd door een mutex.
// Inhoud is een '\0'-terminated C string
typedef struct
{
	char buffer[RECVBUFFERSIZE]; // Daadwerkelijke JSON
	unsigned int msgNumber;      // Volgnummer
} SOLATBOAT_INFO_STRUCT;
pthread_mutex_t mutex;

// Globals (shared info tussen meerdere threads, mutexed)
SOLATBOAT_INFO_STRUCT solarboatInfo;


//PROTO'S!!
void *handleSolarBoatServer(void *arg);


#endif

