#!/usr/bin/shell

echo "Setting up /dev/ttyS3 to baud rate 4800"
stty -F /dev/ttyS3 4800

echo "Done setting up /dev/ttyS3 to baud rate 4800"

echo "Now lets start the GPS / GPRS Telit module."
./toggleTelitOnOff
echo "The Telit GPS / GPRS module is toggled in activity. (It is most likely on now)"

echo "Starting with the initialization of the GPRS module."
route del default gw 192.168.1.1 eth0
echo "Deleted the gateway adress for eth0."
. /etc/ppp/ppp-start
sleep 5
echo "Done with initializing the GPRS module"
