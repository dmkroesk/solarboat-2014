cdprompt ()
{
	cd $*
	PS1="[$LOGNAME@$HOSTNAME $PWD]$$"
	if [ `id -u` = 0 ]; then
		PS1=$PS1"# "
	else
		PS1=$PS1"$ "
	fi
}

case "$-" in
	*i*)
		# Use it
		alias cd=cdprompt

		# Initialise PS1
		cdprompt `pwd`
esac
