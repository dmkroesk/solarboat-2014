/* Google map data */
var map;
var markers = [];
var boatPathPoints = [];
var boatPath;
var longitudeData = [];
var latitudeData = [];
var startDate;
var endDate;
var livePath;
var livePathInitialized = false;

/* Latest user location data */
var userLastLatitude;
var userLastLongitude;
var userLocationMarker;

/* Latest user data */
var userLastTimeStamp;

/* Button data */
var liveUpdateUserLocation = false;
var showRoute = false;
var showRouteMarkers = false;


var firstLoaded = false;
var noLiveData = false;
var noLiveNoty;
var unableToLoadNoty;

/* Debugging variables */
var setDayOne;
var setDayTwo;

$(document).ready(function()
{
    /****************************/
    /* Start initialize methods */
    /****************************/
    initializeMap();
	
	var todayDate = new Date();
	
	$('#startDateDatetimepicker').val(
		todayDate.getFullYear() + 
		"/" + 
		(((todayDate.getMonth() + 1) < 10) ? ("0" + (todayDate.getMonth() + 1)) : (todayDate.getMonth() + 1)) +
		"/" + 
		((todayDate.getDate() < 10) ? ("0" + todayDate.getDate()) : todayDate.getDate()) +
		" 00:00"
	);
	
	$('#endDateDatetimepicker').val(
		todayDate.getFullYear() +
		"/" + 
		(((todayDate.getMonth() + 1) < 10) ? ("0" + (todayDate.getMonth() + 1)) : (todayDate.getMonth() + 1)) + 
		"/" + 
		(((todayDate.getDate() + 1) < 10) ? ("0" + (todayDate.getDate() + 1)) : (todayDate.getDate() + 1)) +
		" 00:00"
	);	
		
	setDayOne = function setDayOne()
	{
		$('#startDateDatetimepicker').val("2014/06/29 00:00");
		$('#endDateDatetimepicker').val("2014/06/29 18:00");
		
		startDate = new Date($('#startDateDatetimepicker').val());
		startDate.setHours(startDate.getHours() + 2);
		startDate = startDate.toISOString();
		
		endDate = new Date($('#endDateDatetimepicker').val());
		endDate.setHours(endDate.getHours() + 2);
		endDate = endDate.toISOString();
		
		getData();
	}
	
	setDayTwo = function setDateTwo()
	{
		$('#startDateDatetimepicker').val("2014/06/30 00:00");
		$('#endDateDatetimepicker').val("2014/06/30 20:00");
		
		startDate = new Date($('#startDateDatetimepicker').val());
		startDate.setHours(startDate.getHours() + 2);
		startDate = startDate.toISOString();
		
		endDate = new Date($('#endDateDatetimepicker').val());
		endDate.setHours(endDate.getHours() + 2);
		endDate = endDate.toISOString();
		
		getData();	
	}
	
    function initializeMap()
    {
        var mapOptions =
		{
			center: new google.maps.LatLng(52.2390342, 6.0168789),
			zoom: 8
		};

        map = new google.maps.Map($("#map-canvas")[0], mapOptions);
    }
    /**************************/
    /* End initialize methods */
    /**************************/

	
	/***********************/
    /* Start timer methods */
    /***********************/
	var dataTimer = $.timer(function()
    {
		getUserLastData();
    });
	dataTimer.set({time: 1000, autostart:true});
	
	/*********************/
    /* End timer methods */
    /*********************/

    /*************************/
    /* Start click listeners */
    /*************************/
    $("#showRoute").click(function()
    {
        showRoute = !showRoute;

        if (showRoute)
        {
            $("#showRoute").html("Hide route");
            $("#showRoute").prop("disabled", true);
			showRouteChildItems();
            getData();
        }
        else
        {
            $("#showRoute").html("Show route");
			hideRouteChildItems();
            removeData();

            if (showRouteMarkers)
                $("#showRouteMarkers").click();
        }
    });

    $("#showRouteMarkers").click(function()
    {
        showRouteMarkers = !showRouteMarkers;

        if (showRouteMarkers)
        {
            $("#showRouteMarkers").html("Hide route markers");
            drawRouteMarkers();
        }
        else
        {
            $("#showRouteMarkers").html("Show route markers");
            hideRouteMarkers();
        }
    });

    $("#updateRouteSelection").click(function()
    {
        getData();
    });
    /***********************/
    /* End click listeners */
    /***********************/


    /*************************/
    /* Start datetimepickers */
    /*************************/
    $("#startDateDatetimepicker").datetimepicker({
        onShow: function() {
            var date = $("#endDateDatetimepicker").val().substring(0, $("#endDateDatetimepicker").val().length - 6);

            this.setOptions({
                maxDate: date ? date : false
            });
        },
        onChangeDateTime: function(dp, $input) {
			if($("#startDateDatetimepicker").val().trim() != "")
			{
				startDate = new Date($input.val());
				startDate.setHours(startDate.getHours() + 2);
				startDate = startDate.toISOString();
			}
        }
    });

    $("#endDateDatetimepicker").datetimepicker({
        onShow: function() {
            var date = $("#startDateDatetimepicker").val().substring(0, $("#startDateDatetimepicker").val().length - 6);

            this.setOptions({
                minDate: date ? date : false
            });
        },
        onChangeDateTime: function(dp, $input) {
			if($("#endDateDatetimepicker").val().trim() != "")
			{
				endDate = new Date($input.val());
				endDate.setHours(endDate.getHours() + 2);
				endDate = endDate.toISOString();
			}
        }
    });
    /*************************/
    /* End datetimepickers */
    /*************************/


    /***********************/
    /* Start route drawing */
    /***********************/
    function getData()
    {
        if (showRouteMarkers)
            hideRouteMarkers();

        if (undefined === startDate && undefined === endDate)
        {
            startDate = new Date($("#startDateDatetimepicker").val());
			startDate.setHours(startDate.getHours() + 2);
            startDate = startDate.toISOString();

            endDate = new Date($("#endDateDatetimepicker").val());
			endDate.setHours(endDate.getHours() + 2);
            endDate = endDate.toISOString();
        }
		
		if($("#startDateDatetimepicker").val().trim() == "" || $("#endDateDatetimepicker").val().trim() == "")
			alert("Faulty date selection!\nVerify your selection!");
        else if ((new Date(endDate) - new Date(startDate)) / 1000 / 60 <= 0)
            alert("Faulty date selection!\nVerify your selection!");
        else if ((new Date(endDate) - new Date(startDate)) / 1000 / 60 > 525600)
            alert("Faulty date selection!\nSelection can't be over a year!");
        else
        {	
            if (undefined !== boatPath)
                removeData();

			latitudeData = [];
			longitudeData = [];
			
			$.getJSON("http://avansti.nl/api/solar/?start=" + startDate + "&end=" + endDate,
				function(receivedTimeData) 
				{
					timeData = receivedTimeData;				
					
					$.each(timeData, function(index, d)
					{
						if(d.latitude != "" && d.longitude != "")
						{
							latitudeData.push(formatLatitudeCoordinate(d.latitude));
							longitudeData.push(formatLongitudeCoordinate(d.longitude));
						}
					});
					
					drawBoatRoute();
				}
			);
        }
    }

    function drawBoatRoute()
    {
        if (undefined !== longitudeData && longitudeData.length > 0)
        {
            var lowestVarLength = latitudeData.length;
			
            if (longitudeData.length < lowestVarLength)
                lowestVarLength = longitudeData.length;

            for (var i = 0; i < lowestVarLength; i++)
				boatPathPoints.push(new google.maps.LatLng(latitudeData[i], longitudeData[i]));

            boatPath = new google.maps.Polyline(
                    {
                        path: boatPathPoints,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });

            boatPath.setMap(map);

            if (showRouteMarkers)
                drawRouteMarkers();
        }

        showRouteChildItems();
    }

    function showRouteChildItems()
    {
        $("#showRouteMarkers").css("visibility", "visible");
        $(".datePickerText").css("visibility", "visible");
        $("#startDateDatetimepicker").css("visibility", "visible");
        $("#endDateDatetimepicker").css("visibility", "visible");
        $("#updateRouteSelection").css("visibility", "visible");
        $("#showRoute").prop("disabled", false);
    }

    function hideRouteChildItems()
    {
        $("#showRouteMarkers").css("visibility", "hidden");
        $(".datePickerText").css("visibility", "hidden");
        $("#startDateDatetimepicker").css("visibility", "hidden");
        $("#endDateDatetimepicker").css("visibility", "hidden");
        $("#updateRouteSelection").css("visibility", "hidden");
    }

    function drawRouteMarkers()
    {
        var lowestVarLength = latitudeData.length;
	
        if (longitudeData.length < lowestVarLength)
            lowestVarLength = longitudeData.length;

        for (var i = 0; i < lowestVarLength; i++)		
			addMarker(latitudeData[i], longitudeData[i]);
    }

    function hideRouteMarkers()
    {
        for (var i = 0; i < markers.length; i++)
            markers[i].setMap(null);
    }

    function addMarker(latitude, longitude)
    {
        var myLatlng = new google.maps.LatLng(latitude, longitude);

        var marker = new google.maps.Marker(
                {
                    position: myLatlng,
                    map: map
                });

        markers.push(marker);
    }

    function removeData()
    {
        boatPath.setMap(null);
        delete boatPath;
        boatPathPoints = [];
    }
    /*********************/
    /* End route drawing */
    /*********************/


    /**************************************/
    /* Start latest boat location methods */
    /**************************************/
    $("#liveUpdate").click(function()
    {
        liveUpdateUserLocation = !liveUpdateUserLocation;

        if (liveUpdateUserLocation)
        {
            $("#liveUpdate").html("Disable live position update");
            $('.autoCenterMap').css("visibility", "visible");
			$('.autoDrawRoute').css("visibility", "visible");
        }
        else
        {
            $("#liveUpdate").html("Enable live position update");
            $('.autoCenterMap').css("visibility", "hidden");
			$('.autoDrawRoute').css("visibility", "hidden");
        }
    });

    function drawLatestUserLocation()
    {
        if (undefined !== userLocationMarker)
            updateLatestUserLocationMarkerPosition();
        else
        {
            var userLocation = new google.maps.LatLng(userLastLatitude, userLastLongitude);

            var image = 'img/marker.png';

            userLocationMarker = new google.maps.Marker(
                    {
                        position: userLocation,
                        icon: image
                    });

            userLocationMarker.setMap(map);
            map.panTo(userLocation);
            map.setZoom(14);
        }
    }

    function updateLatestUserLocationMarkerPosition()
    {
        if (liveUpdateUserLocation)
        {
            var userLocation = new google.maps.LatLng(userLastLatitude, userLastLongitude);
            if ($('#autoCenterMap').prop('checked'))
                map.panTo(userLocation);
            userLocationMarker.setPosition(userLocation);
			
			if ($('#autoDrawRoute').prop('checked'))
			{
				if(!livePathInitialized)
				{
					// console.log("Initializing livePath");
					livePath = new google.maps.Polyline(
					{
						geodesic: true,
						strokeColor: '#FF0000',
						strokeOpacity: 1.0,
						strokeWeight: 2
					});		
					
					// console.log("Pushing intial LatLon data");
					var path = livePath.getPath();
					path.push(new google.maps.LatLng(userLastLatitude, userLastLongitude));
					
					// console.log("Setting map livePath");
					livePath.setMap(map)

					livePathInitialized = true;
				}
				else
				{
					// console.log("Updating livePath");
					var path = livePath.getPath();
					path.push(new google.maps.LatLng(userLastLatitude, userLastLongitude));
				}
			}
        }
    }
	
	$("#autoDrawRoute").change(function() 
	{
		if(!this.checked) 
		{
			livePathInitialized = false;
		
			livePath.setMap(null);
			delete livePath;
		}
	});
    /************************************/
    /* End latest boat location methods */
    /************************************/


    /**********************************/
    /* Start lat & lon format methods */
    /**********************************/
    function formatLatitudeCoordinate(latitude)
    {
        var integer = String(latitude).substring(0, 2);
		
        latitude = latitude / 100;
        latitude = latitude - parseInt(integer);
        latitude = (latitude / 60) * 100;
        latitude = latitude + parseInt(integer);
		
        return latitude;
    }

    function formatLongitudeCoordinate(longitude)
    {
        var integer = String(longitude).substring(0, 3);

        longitude = longitude / 100;
        longitude = longitude - parseInt(integer);
        longitude = (longitude / 60) * 100;
        longitude = longitude + parseInt(integer);

        return longitude;
    }
    /********************************/
    /* End lat & lon format methods */
    /********************************/
	
	/*****************************/
    /* Start datareceive methods */
    /*****************************/
	
	function getUserLastData()
    {
		//Get data from server.
		$.getJSON("http://avansti.nl/api/solar?key=last",
		// $.getJSON("http://avansti.nl/js/test.js",
			function(data) 
			{		
				if(data[0].frozen == "False") 
				{
					if(noLiveData)
					{
						noLiveData = false;
						
						if(undefined !== noLiveNoty)
							noLiveNoty.close();
					}
				
					if(data[0].foxtimestamp != userLastTimeStamp)
					{
						userLastTimeStamp = data[0].foxtimestamp;

						updateData(data);	
						
						if(liveUpdateUserLocation || !firstLoaded)
						{
							if((data[0].latitude && data[0].longitude) && (data[0].latitude != 0 && data[0].longitude != 0))
							{
								// console.log("Foxboard location");
							
								if(undefined !== unableToLoadNoty)
									unableToLoadNoty.close();
							
								userLastLatitude = formatLatitudeCoordinate(data[0].latitude);
								userLastLongitude = formatLongitudeCoordinate(data[0].longitude);
								
								drawLatestUserLocation();
							}
							else if((data[0].GPSLatitude && data[0].GPSLongtitude) && (data[0].GPSLatitude != 0 && data[0].GPSLongtitude != 0))
							{
								// console.log("MobiBoxx location");
							
								if(undefined !== unableToLoadNoty)
									unableToLoadNoty.close();
							
								userLastLatitude = data[0].GPSLatitude;
								userLastLongitude = data[0].GPSLongtitude;	
								
								drawLatestUserLocation();
							}
							else
							{
								unableToLoadNoty = noty({
									text        : 'Unable to load live position boat, last known location will be shown.',
									type        : 'error',
									dismissQueue: true,
									layout      : 'topCenter',
									theme       : 'defaultTheme',
									maxVisible  : 10
								});
								
								findLastUserLocation();	
							}

							firstLoaded = true;
						}
					}
				}
				else
				{
					if(undefined !== unableToLoadNoty)
						unableToLoadNoty.close();

					if(!noLiveData)
					{
						findLastUserLocation();
						
						noLiveData = true;
						
						noLiveNoty = noty({
							text        : 'No live boat data available, showing last known location.',
							type        : 'error',
							dismissQueue: true,
							layout      : 'topCenter',
							theme       : 'defaultTheme',
							maxVisible  : 10
						});
					}
					
					$("#speed").html( "...");
					$("#course").html( "...");
					$("#date").html( "Geen live data");
					$("#time").html( "...");
				}
			}
		);
    }
	
	function findLastUserLocation()
	{
		$.getJSON("http://avansti.nl/api/solar/values?lastnumber=200",
			function(data) 
			{		
				for (var i = 0; i < data.length; i++)
				{
					// console.log(data[i]);
					// console.log("Foxboard location: " + formatLatitudeCoordinate(data[i].latitude) + "(" + (data[i].latitude) + ")" + " - " + formatLongitudeCoordinate(data[i].longitude));
					// console.log("MobiBox location: " + data[i].GPSLatitude + " - " + data[i].GPSLongtitude);
				
					if((data[i].latitude && data[i].longitude) && (data[i].latitude != 0 && data[i].longitude != 0))
					{
						// console.log("Using Foxboard location");
						userLastLatitude = formatLatitudeCoordinate(data[i].latitude);
						userLastLongitude = formatLongitudeCoordinate(data[i].longitude);
						break;
					}
					else if((data[i].GPSLatitude && data[i].GPSLongtitude) && (data[i].GPSLatitude != 0 && data[i].GPSLongtitude != 0))
					{
						// console.log("Using MobiBoxx location");
						userLastLatitude = data[i].GPSLatitude;
						userLastLongitude = data[i].GPSLongtitude;	
						break;
					}
				}
				
				drawLatestUserLocation();	
			}
		);
	}
	
	function updateData(data)
	{
		$("#speed").html(data[0].groundspeed + " m/s");
		$("#course").html(data[0].course + "&#176");
		$("#date").html(data[0].foxtimestamp.split("T")[0]);
		$("#time").html(data[0].foxtimestamp.split("T")[1]);
	}
	
	/***************************/
    /* End datareceive methods */
    /***************************/
});
