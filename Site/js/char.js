$(document).ready(function()
{
	var data1 = [];
	var data2 = [];	
	var data3 = [];
	var data4 = [];
	var legends = $("#placeholder .legendLabel");

		legends.each(function () {
			// fix the widths so they don't jump around
			$(this).css('width', $(this).width());
		});
	
	var updateLegendTimeout = null;
	var latestPosition = null;
	var plot;
	
	start();

	function start(){
			data1 = [];
			data2 = [];
			$.getJSON("http://avansti.nl/api/solar/?lastnumber=20",
				function(receivedData) 
				{	
					$.each(receivedData, function(index, d)
					{
						var time = Date.parse(d.foxtimestamp);
						data1.push([time, d.BMSHighTemp]);
						data2.push([time, d.BMSLowTemp]);
					});
					plotting2("BMSHighTemp", "BMSLowTemp");
				}
			);
	}
	
		//BMS temperature
		$("#BMSTemp").click(function () {
			data1 = [];
			data2 = [];
			data3 = [];
			data4 = [];
			$.getJSON("http://avansti.nl/api/solar/?lastnumber=20",
				function(receivedData) 
				{	
					$.each(receivedData, function(index, d)
					{
						var time = Date.parse(d.foxtimestamp);
						data1.push([time, d.BMSHighTemp]);
						data2.push([time, d.BMSLowTemp]);
					});
					plotting2("BMSHighTemp", "BMSLowTemp");
				}
			);
			
			
		});

		//BMS Cell Voltage
		$("#BMSCellVoltage").click(function () {
			data1 = [];
			data2 = [];
			data3 = [];
			data4 = [];
			$.getJSON("http://avansti.nl/api/solar/?lastnumber=20",
				function(receivedData) 
				{	
					$.each(receivedData, function(index, d)
					{
						var time = Date.parse(d.foxtimestamp);
						data1.push([time, d.BMSAvgCellVoltage]);
						data2.push([time, d.BMSHighCellVoltage]);
					});

					plotting2("BMSAvgCellVoltage", "BMSHighCellVoltage");
				}
			);
		});

		//voltage in MPPT
		$("#voltageIn").click(function () {
			data1 = [];
			data2 = [];
			data3 = [];
			data4 = [];
			$.getJSON("http://avansti.nl/api/solar/?lastnumber=20",
				function(receivedData) 
				{	
					$.each(receivedData, function(index, d)
					{
						var time = Date.parse(d.foxtimestamp);
						data1.push([time, d.MPPT1VoltageIn]);
						data2.push([time, d.MPPT2VoltageIn]);
						data3.push([time, d.MPPT3VoltageIn]);
						data4.push([time, d.MPPT4VoltageIn]);
					});
					plotting4("Voltage in MPPT1", "Voltage in MPPT2", "Voltage in MPPT3", "Voltage in MPPT4");
				}
			);
		});
		
		//voltage out MPPT
		$("#voltageOut").click(function () {
			data1 = [];
			data2 = [];
			data3 = [];
			data4 = [];
			$.getJSON("http://avansti.nl/api/solar/?lastnumber=20",
				function(receivedData) 
				{	
					$.each(receivedData, function(index, d)
					{
						var time = Date.parse(d.foxtimestamp);
						data1.push([time, d.MPPT1VoltageOut]);
						data2.push([time, d.MPPT2VoltageOut]);
						data3.push([time, d.MPPT3VoltageOut]);
						data4.push([time, d.MPPT4VoltageOut]);
					});
					plotting4("Voltage out MPPT1", "Voltage out MPPT2", "Voltage out MPPT3", "Voltage out MPPT4");
				}
			);
		});
		
		//amps in MPPT
		$("#ampsIn").click(function () {
			data1 = [];
			data2 = [];
			data3 = [];
			data4 = [];
			$.getJSON("http://avansti.nl/api/solar/?lastnumber=20",
				function(receivedData) 
				{	
					$.each(receivedData, function(index, d)
					{
						var time = Date.parse(d.foxtimestamp);
						data1.push([time, d.MPPT1AmpsIn]);
						data2.push([time, d.MPPT2AmpsIn]);
						data3.push([time, d.MPPT3AmpsIn]);
						data4.push([time, d.MPPT4AmpsIn]);
					});
					plotting4("Amps In MPPT1", "Amps In MPPT2", "Amps In MPPT3", "Amps In MPPT4");
				}
			);
		});
		
		
		function updateLegend() {
			
			updateLegendTimeout = null;

			var pos = latestPosition;

			var axes = plot.getAxes();
			
			if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
				pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) {
				return;
			}
			
			
			var i, j, dataset = plot.getData();
			for (i = 0; i < dataset.length; ++i) {

				var series = dataset[i];

				// Find the nearest points, x-wise

				for (j = 0; j < series.data.length; ++j) {
					if (series.data[j][0] > pos.x) {
						break;
					}
				}

				// Now Interpolate

				var y,
					p1 = series.data[j - 1],
					p2 = series.data[j];

				if (p1 == null) {
					y = p2[1];
				} else if (p2 == null) {
					y = p1[1];
				} else {
					y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);
				}
				y = parseFloat(y);
				console.log(y);
				console.log(legends.eq(i));
				legends.eq(i).text(series.label.replace(/=.*/, "= " + y.toFixed(2)));
			}
		}

		$("#placeholder").bind("plothover",  function (event, pos, item) {
			latestPosition = pos;
			if (!updateLegendTimeout) {
				updateLegendTimeout = setTimeout(updateLegend, 50);
			}
		});
		
		function plotting2(label1, label2) {
			plot = $.plot("#placeholder", [
				{ data: data1, label: label1 + " = -0.00"},
				{ data: data2, label: label2 + " = -0.00" }], {
					xaxis: { mode: "time" }, 
					series: {
						lines: {
							show: true
						}
					},
					crosshair: {
						mode: "x"
					},
					grid: {
						hoverable: true,
						autoHighlight: false
					},
					legend: { position: "sw" }
				});
		}
		
		function plotting4(label1, label2, label3, label4) {
			plot = $.plot("#placeholder", [
				{ data: data1, label: label1 + " = -0.00"},
				{ data: data2, label: label2 + " = -0.00" },
				{ data: data3, label: label3 + " = -0.00"},
				{ data: data4, label: label4 + " = -0.00" }], {
					xaxis: { mode: "time" }, 
					series: {
						lines: {
							show: true
						}
					},
					crosshair: {
						mode: "x"
					},
					grid: {
						hoverable: true,
						autoHighlight: false
					},
					legend: { position: "sw" }
				});
		}
		
});



