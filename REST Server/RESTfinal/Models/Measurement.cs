﻿using System;
using Newtonsoft.Json;

namespace RESTfinal.Models
{


    public class Measurement
    {
        public int Id { get; set; }

        [JsonProperty("timestamp")]
        public String timeStamp { get; set; }

        [JsonProperty("temperature")]
        public String temperature { get; set; }

        [JsonProperty("longitude")]
        public String longitude { get; set; }

        [JsonProperty("latitude")]
        public String latitude { get; set; }

        [JsonProperty("groundspeed")]
        public String groundSpeed { get; set; }

        [JsonProperty("course")]
        public String course { get; set; }

        [JsonProperty("northsouth")]
        public String northsouth { get; set; }

        [JsonProperty("eastwest")]
        public String eastwest { get; set; }

        [JsonProperty("foxtimestamp")]
        public String foxtimestamp { get; set; }

        [JsonProperty("ipadress")]
        public String ipadress { get; set; }

        [JsonProperty("MPPT2AmpsIn")]
        public String MPPT2AmpsIn { get; set; }

        [JsonProperty("BMSStateOfCharge")]
        public String BMSStateOfCharge { get; set; }

        [JsonProperty("BMSPackAmpHours")]
        public String BMSPackAmpHours { get; set; }

        [JsonProperty("MPPT2NoCharge")]
        public String MPPT2NoCharge { get; set; }

        [JsonProperty("motorControllerMotorCurrent")]
        public String motorControllerMotorCurrent { get; set; }

        [JsonProperty("BMSHighCellVoltage")]
        public String BMSHighCellVoltage { get; set; }

        [JsonProperty("MPPT2VoltageIn")]
        public String MPPT2VoltageIn { get; set; }

        [JsonProperty("BMSHighTemp")]
        public String BMSHighTemp { get; set; }

        [JsonProperty("MPPT4NoCharge")]
        public String MPPT4NoCharge { get; set; }

        [JsonProperty("MPPT4VoltageIn")]
        public String MPPT4VoltageIn { get; set; }

        [JsonProperty("throttlePosition")]
        public String throttlePosition { get; set; }

        [JsonProperty("BMSSummedVoltage")]
        public String BMSSummedVoltage { get; set; }

        [JsonProperty("BMSAvgCellVoltage")]
        public String BMSAvgCellVoltage { get; set; }

        [JsonProperty("motorControllerSetSpeed")]
        public String motorControllerSetSpeed { get; set; }

        [JsonProperty("throttleVoltage")]
        public String throttleVoltage { get; set; }

        [JsonProperty("MPPT3VoltageIn")]
        public String MPPT3VoltageIn { get; set; }

        [JsonProperty("MPPT4TemperatureFlag")]
        public String MPPT4TemperatureFlag { get; set; }

        [JsonProperty("GPSSpeed")]
        public String GPSSpeed { get; set; }

        [JsonProperty("MPPT3UnderVoltage")]
        public String MPPT3UnderVoltage { get; set; }

        [JsonProperty("GPSLatitude")]
        public String GPSLatitude { get; set; }

        [JsonProperty("MPPT4VoltageFlag")]
        public String MPPT4VoltageFlag { get; set; }

        [JsonProperty("MPPT4UnderVoltage")]
        public String MPPT4UnderVoltage { get; set; }

        [JsonProperty("MPPT3NoCharge")]
        public String MPPT3NoCharge { get; set; }

        [JsonProperty("MPPT3TemperatureFlag")]
        public String MPPT3TemperatureFlag { get; set; }

        [JsonProperty("GPSSattelites")]
        public String GPSSattelites { get; set; }

        [JsonProperty("MPPT1VoltageOut")]
        public String MPPT1VoltageOut { get; set; }

        [JsonProperty("GPSLongtitude")]
        public String GPSLongtitude { get; set; }

        [JsonProperty("motorControllerCommandWord")]
        public String motorControllerCommandWord { get; set; }

        [JsonProperty("MPPT1VoltageIn")]
        public String MPPT1VoltageIn { get; set; }

        [JsonProperty("motorControllerMotorTemp")]
        public String motorControllerMotorTemp { get; set; }

        [JsonProperty("BMSPackCurrent")]
        public String BMSPackCurrent { get; set; }

        [JsonProperty("MPPT3VoltageFlag")]
        public String MPPT3VoltageFlag { get; set; }

        [JsonProperty("MPPT1VoltageFlag")]
        public String MPPT1VoltageFlag { get; set; }

        [JsonProperty("motorControllerBusVoltage")]
        public String motorControllerBusVoltage { get; set; }

        [JsonProperty("GPSAltitude")]
        public String GPSAltitude { get; set; }

        [JsonProperty("MPPT4VoltageOut")]
        public String MPPT4VoltageOut { get; set; }

        [JsonProperty("MPPT1UnderVoltage")]
        public String MPPT1UnderVoltage { get; set; }

        [JsonProperty("MPPT4AmpsIn")]
        public String MPPT4AmpsIn { get; set; }

        [JsonProperty("BMSLowCellVoltage")]
        public String BMSLowCellVoltage { get; set; }

        [JsonProperty("logs")]
        public String logs { get; set; }

        [JsonProperty("MPPT1AmpsIn")]
        public String MPPT1AmpsIn { get; set; }

        [JsonProperty("MPPT2UnderVoltage")]
        public String MPPT2UnderVoltage { get; set; }

        [JsonProperty("MPPT1NoCharge")]
        public String MPPT1NoCharge { get; set; }

        [JsonProperty("MPPT1TemperatureFlag")]
        public String MPPT1TemperatureFlag { get; set; }

        [JsonProperty("motorControllerActualSpeed")]
        public String motorControllerActualSpeed { get; set; }

        [JsonProperty("MPPT3AmpsIn")]
        public String MPPT3AmpsIn { get; set; }

        [JsonProperty("MPPT2VoltageOut")]
        public String MPPT2VoltageOut { get; set; }

        [JsonProperty("MPPT2VoltageFlag")]
        public String MPPT2VoltageFlag { get; set; }

        [JsonProperty("MPPT2TemperatureFlag")]
        public String MPPT2TemperatureFlag { get; set; }

        [JsonProperty("throttleCentered")]
        public String throttleCentered { get; set; }

        [JsonProperty("MPPT3VoltageOut")]
        public String MPPT3VoltageOut { get; set; }

        [JsonProperty("BMSLowTemp")]
        public String BMSLowTemp { get; set; }

        [JsonProperty("frozen")]
        public String frozen { get; set; }

        [JsonProperty("speed")]
        public String speed { get; set; }

        public Measurement()
        {
            temperature = "";
            longitude = "";
            latitude = "";
            groundSpeed = "";
            course = "";
            northsouth = "";
            eastwest = "";
            foxtimestamp = "";
            ipadress = "";
            MPPT2AmpsIn = "";
            BMSStateOfCharge = "";
            BMSPackAmpHours = "";
            MPPT2NoCharge = "";
            motorControllerMotorCurrent = "";
            BMSHighCellVoltage = "";
            MPPT2VoltageIn = "";
            BMSHighTemp = "";
            MPPT4NoCharge = "";
            MPPT4VoltageIn = "";
            throttlePosition = "";
            BMSSummedVoltage = "";
            BMSAvgCellVoltage = "";
            motorControllerSetSpeed = "";
            throttleVoltage = "";
            MPPT3VoltageIn = "";
            MPPT4TemperatureFlag = "";
            GPSSpeed = "";
            MPPT3UnderVoltage = "";
            GPSLatitude = "";
            MPPT4VoltageFlag = "";
            MPPT4UnderVoltage = "";
            MPPT3NoCharge = "";
            MPPT3TemperatureFlag = "";
            GPSSattelites = "";
            MPPT1VoltageOut = "";
            GPSLongtitude = "";
            motorControllerCommandWord = "";
            MPPT1VoltageIn = "";
            motorControllerMotorTemp = "";
            BMSPackCurrent = "";
            MPPT3VoltageFlag = "";
            MPPT1VoltageFlag = "";
            motorControllerBusVoltage = "";
            GPSAltitude = "";
            MPPT4VoltageOut = "";
            MPPT1UnderVoltage = "";
            MPPT4AmpsIn = "";
            BMSLowCellVoltage = "";
            logs = "";
            MPPT1AmpsIn = "";
            MPPT2UnderVoltage = "";
            MPPT1NoCharge = "";
            MPPT1TemperatureFlag = "";
            motorControllerActualSpeed = "";
            MPPT3AmpsIn = "";
            MPPT2VoltageOut = "";
            MPPT2VoltageFlag = "";
            MPPT2TemperatureFlag = "";
            throttleCentered = "";
            MPPT3VoltageOut = "";
            BMSLowTemp = "";
            frozen = "";
            speed = "";


        }

    }


}