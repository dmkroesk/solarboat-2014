﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using RESTfinal.Models;
using MySql.Data.MySqlClient;
using RESTfinal.Models;



namespace RESTfinal.Controllers
{
    public class SolarController : ApiController
    {
        private MySqlConnection _mySqlConnection = new MySqlConnection("server=127.0.0.1;user=root;database=solardb;port=3306;password=");
        private MySqlCommand _cmd = null;

        public string Get()
        {
            return null;
        }

        public List<Measurement> Get([FromUri]string key)
        {
          var list = new List<Measurement>();

            switch (key)
            {
                /**********************************/
                /* laatste database entry ophalen */
                /**********************************/
                //http://avansti.nl/api/solar/?key=last
                #region last
               
                case "last":
                    try
                    {
                        _mySqlConnection.Open();
                        String sqlstat = "SELECT " + Support.alleVelden + " FROM Measurement ORDER BY id DESC LIMIT 1";
                        using (MySqlCommand command = new MySqlCommand(sqlstat, _mySqlConnection))
                        {
                            MySqlDataReader reader = command.ExecuteReader();
                     
                            while (reader.Read())
                                list.Add(new Measurement
                                {
                                    Id = reader.GetInt32(0),
                                    temperature = reader.GetString(1),
                                    longitude = reader.GetString(2),
                                    latitude = reader.GetString(3),
                                    groundSpeed = reader.GetString(4),
                                    course = reader.GetString(5),
                                    northsouth = reader.GetString(6),
                                    eastwest = reader.GetString(7),
                                    foxtimestamp = reader.GetString(8),
                                    ipadress = reader.GetString(9),
                                    MPPT2AmpsIn = reader.GetString(10),
                                    BMSStateOfCharge = reader.GetString(11),
                                    BMSPackAmpHours = reader.GetString(12),
                                    MPPT2NoCharge = reader.GetString(13),
                                    motorControllerMotorCurrent = reader.GetString(14),
                                    BMSHighCellVoltage = reader.GetString(15),
                                    MPPT2VoltageIn = reader.GetString(16),
                                    BMSHighTemp = reader.GetString(17),
                                    MPPT4NoCharge = reader.GetString(18),
                                    MPPT4VoltageIn = reader.GetString(19),
                                    throttlePosition = reader.GetString(20),
                                    BMSSummedVoltage = reader.GetString(21),
                                    BMSAvgCellVoltage = reader.GetString(22),
                                    motorControllerSetSpeed = reader.GetString(23),
                                    throttleVoltage = reader.GetString(24),
                                    MPPT3VoltageIn = reader.GetString(25),
                                    MPPT4TemperatureFlag = reader.GetString(26),
                                    GPSSpeed = reader.GetString(27),
                                    MPPT3UnderVoltage = reader.GetString(28),
                                    GPSLatitude = reader.GetString(29),
                                    MPPT4VoltageFlag = reader.GetString(30),
                                    MPPT4UnderVoltage = reader.GetString(31),
                                    MPPT3NoCharge = reader.GetString(32),
                                    MPPT3TemperatureFlag = reader.GetString(33),
                                    GPSSattelites = reader.GetString(34),
                                    MPPT1VoltageOut = reader.GetString(35),
                                    GPSLongtitude = reader.GetString(36),
                                    motorControllerCommandWord = reader.GetString(37),
                                    MPPT1VoltageIn = reader.GetString(38),
                                    motorControllerMotorTemp = reader.GetString(39),
                                    BMSPackCurrent = reader.GetString(40),
                                    MPPT3VoltageFlag = reader.GetString(41),
                                    MPPT1VoltageFlag = reader.GetString(42),
                                    motorControllerBusVoltage = reader.GetString(43),
                                    GPSAltitude = reader.GetString(44),
                                    MPPT4VoltageOut = reader.GetString(45),
                                    MPPT1UnderVoltage = reader.GetString(46),
                                    MPPT4AmpsIn = reader.GetString(47),
                                    BMSLowCellVoltage = reader.GetString(48),
                                    logs = reader.GetString(49),
                                    MPPT1AmpsIn = reader.GetString(50),
                                    MPPT2UnderVoltage = reader.GetString(51),
                                    MPPT1NoCharge = reader.GetString(52),
                                    MPPT1TemperatureFlag = reader.GetString(53),
                                    motorControllerActualSpeed = reader.GetString(54),
                                    MPPT3AmpsIn = reader.GetString(55),
                                    MPPT2VoltageOut = reader.GetString(56),
                                    MPPT2VoltageFlag = reader.GetString(57),
                                    MPPT2TemperatureFlag = reader.GetString(58),
                                    throttleCentered = reader.GetString(59),
                                    MPPT3VoltageOut = reader.GetString(60),
                                    BMSLowTemp = reader.GetString(61),
                                    frozen = Support.isFrozen(reader.GetString(8)),
                                    speed = reader.GetString(63),
                                    timeStamp = DateTime.Now.ToString()


                                });
                          
                           

                        }

                    }
                    catch (Exception)
                    {
                       
                    }
                    _mySqlConnection.Close();
                    break;
                #endregion

                /****************************/
                /* Alle waarden ophalen     */
                /****************************/
                //http://avansti.nl/api/solar/?key=all
                #region all
                
                case "all":
                    try
                    {
                        _mySqlConnection.Open();
                        String sqlstat = "SELECT " + Support.alleVelden + " FROM Measurement";
                        using (MySqlCommand command = new MySqlCommand(sqlstat, _mySqlConnection))
                        {
                            MySqlDataReader reader = command.ExecuteReader();
                           
                            while (reader.Read())
                                list.Add(new Measurement
                                {
                                    Id = reader.GetInt32(0),
                                    temperature = reader.GetString(1),
                                    longitude = reader.GetString(2),
                                    latitude = reader.GetString(3),
                                    groundSpeed = reader.GetString(4),
                                    course = reader.GetString(5),
                                    northsouth = reader.GetString(6),
                                    eastwest = reader.GetString(7),
                                    foxtimestamp = reader.GetString(8),
                                    ipadress = reader.GetString(9),
                                    MPPT2AmpsIn = reader.GetString(10),
                                    BMSStateOfCharge = reader.GetString(11),
                                    BMSPackAmpHours = reader.GetString(12),
                                    MPPT2NoCharge = reader.GetString(13),
                                    motorControllerMotorCurrent = reader.GetString(14),
                                    BMSHighCellVoltage = reader.GetString(15),
                                    MPPT2VoltageIn = reader.GetString(16),
                                    BMSHighTemp = reader.GetString(17),
                                    MPPT4NoCharge = reader.GetString(18),
                                    MPPT4VoltageIn = reader.GetString(19),
                                    throttlePosition = reader.GetString(20),
                                    BMSSummedVoltage = reader.GetString(21),
                                    BMSAvgCellVoltage = reader.GetString(22),
                                    motorControllerSetSpeed = reader.GetString(23),
                                    throttleVoltage = reader.GetString(24),
                                    MPPT3VoltageIn = reader.GetString(25),
                                    MPPT4TemperatureFlag = reader.GetString(26),
                                    GPSSpeed = reader.GetString(27),
                                    MPPT3UnderVoltage = reader.GetString(28),
                                    GPSLatitude = reader.GetString(29),
                                    MPPT4VoltageFlag = reader.GetString(30),
                                    MPPT4UnderVoltage = reader.GetString(31),
                                    MPPT3NoCharge = reader.GetString(32),
                                    MPPT3TemperatureFlag = reader.GetString(33),
                                    GPSSattelites = reader.GetString(34),
                                    MPPT1VoltageOut = reader.GetString(35),
                                    GPSLongtitude = reader.GetString(36),
                                    motorControllerCommandWord = reader.GetString(37),
                                    MPPT1VoltageIn = reader.GetString(38),
                                    motorControllerMotorTemp = reader.GetString(39),
                                    BMSPackCurrent = reader.GetString(40),
                                    MPPT3VoltageFlag = reader.GetString(41),
                                    MPPT1VoltageFlag = reader.GetString(42),
                                    motorControllerBusVoltage = reader.GetString(43),
                                    GPSAltitude = reader.GetString(44),
                                    MPPT4VoltageOut = reader.GetString(45),
                                    MPPT1UnderVoltage = reader.GetString(46),
                                    MPPT4AmpsIn = reader.GetString(47),
                                    BMSLowCellVoltage = reader.GetString(48),
                                    logs = reader.GetString(49),
                                    MPPT1AmpsIn = reader.GetString(50),
                                    MPPT2UnderVoltage = reader.GetString(51),
                                    MPPT1NoCharge = reader.GetString(52),
                                    MPPT1TemperatureFlag = reader.GetString(53),
                                    motorControllerActualSpeed = reader.GetString(54),
                                    MPPT3AmpsIn = reader.GetString(55),
                                    MPPT2VoltageOut = reader.GetString(56),
                                    MPPT2VoltageFlag = reader.GetString(57),
                                    MPPT2TemperatureFlag = reader.GetString(58),
                                    throttleCentered = reader.GetString(59),
                                    MPPT3VoltageOut = reader.GetString(60),
                                    BMSLowTemp = reader.GetString(61),
                                    frozen = Support.isFrozen(reader.GetString(8)),
                                    speed = reader.GetString(63),
                                    timeStamp = DateTime.Now.ToString()


                                });
                           }

                    }
                    catch (Exception)
                    {
                      
                    }
                    _mySqlConnection.Close();
                    break;
                #endregion


                default:
                   
                    break;
            }
            return list;
        }

        /****************************/
        /* Tussen 2 ISO8601 tijden  */
        /****************************/
        //http://avansti.nl/api/solar/?start=[ISO8601]&end=[ISO8601]
        public List<Measurement> Get([FromUri] string start, [FromUri] string end)
        {
            var list = new List<Measurement>();

            try
            {
                _mySqlConnection.Open();
                String sqlstat = "SELECT " + Support.alleVelden + " FROM solardb.Measurement WHERE CAST(foxtimestamp AS DATETIME) BETWEEN CAST('" + start + "' AS DATETIME) AND CAST('" + end + "' AS DATETIME);";
                using (MySqlCommand command = new MySqlCommand(sqlstat, _mySqlConnection))
                {
                    MySqlDataReader reader = command.ExecuteReader();
                   
                    while (reader.Read())
                        list.Add(new Measurement
                        {
                            Id = reader.GetInt32(0),
                            temperature = reader.GetString(1),
                            longitude = reader.GetString(2),
                            latitude = reader.GetString(3),
                            groundSpeed = reader.GetString(4),
                            course = reader.GetString(5),
                            northsouth = reader.GetString(6),
                            eastwest = reader.GetString(7),
                            foxtimestamp = reader.GetString(8),
                            ipadress = reader.GetString(9),
                            MPPT2AmpsIn = reader.GetString(10),
                            BMSStateOfCharge = reader.GetString(11),
                            BMSPackAmpHours = reader.GetString(12),
                            MPPT2NoCharge = reader.GetString(13),
                            motorControllerMotorCurrent = reader.GetString(14),
                            BMSHighCellVoltage = reader.GetString(15),
                            MPPT2VoltageIn = reader.GetString(16),
                            BMSHighTemp = reader.GetString(17),
                            MPPT4NoCharge = reader.GetString(18),
                            MPPT4VoltageIn = reader.GetString(19),
                            throttlePosition = reader.GetString(20),
                            BMSSummedVoltage = reader.GetString(21),
                            BMSAvgCellVoltage = reader.GetString(22),
                            motorControllerSetSpeed = reader.GetString(23),
                            throttleVoltage = reader.GetString(24),
                            MPPT3VoltageIn = reader.GetString(25),
                            MPPT4TemperatureFlag = reader.GetString(26),
                            GPSSpeed = reader.GetString(27),
                            MPPT3UnderVoltage = reader.GetString(28),
                            GPSLatitude = reader.GetString(29),
                            MPPT4VoltageFlag = reader.GetString(30),
                            MPPT4UnderVoltage = reader.GetString(31),
                            MPPT3NoCharge = reader.GetString(32),
                            MPPT3TemperatureFlag = reader.GetString(33),
                            GPSSattelites = reader.GetString(34),
                            MPPT1VoltageOut = reader.GetString(35),
                            GPSLongtitude = reader.GetString(36),
                            motorControllerCommandWord = reader.GetString(37),
                            MPPT1VoltageIn = reader.GetString(38),
                            motorControllerMotorTemp = reader.GetString(39),
                            BMSPackCurrent = reader.GetString(40),
                            MPPT3VoltageFlag = reader.GetString(41),
                            MPPT1VoltageFlag = reader.GetString(42),
                            motorControllerBusVoltage = reader.GetString(43),
                            GPSAltitude = reader.GetString(44),
                            MPPT4VoltageOut = reader.GetString(45),
                            MPPT1UnderVoltage = reader.GetString(46),
                            MPPT4AmpsIn = reader.GetString(47),
                            BMSLowCellVoltage = reader.GetString(48),
                            logs = reader.GetString(49),
                            MPPT1AmpsIn = reader.GetString(50),
                            MPPT2UnderVoltage = reader.GetString(51),
                            MPPT1NoCharge = reader.GetString(52),
                            MPPT1TemperatureFlag = reader.GetString(53),
                            motorControllerActualSpeed = reader.GetString(54),
                            MPPT3AmpsIn = reader.GetString(55),
                            MPPT2VoltageOut = reader.GetString(56),
                            MPPT2VoltageFlag = reader.GetString(57),
                            MPPT2TemperatureFlag = reader.GetString(58),
                            throttleCentered = reader.GetString(59),
                            MPPT3VoltageOut = reader.GetString(60),
                            BMSLowTemp = reader.GetString(61),
                            frozen = Support.isFrozen(reader.GetString(8)),
                            speed = reader.GetString(63),
                            timeStamp = DateTime.Now.ToString()

                        });

                    
                }
            }

            catch (Exception)
            {
                
            }
            _mySqlConnection.Close();
            return list;
        }

        /********************************/
        /* De laatste zoveel (parameter)*/
        /********************************/
        //http://avansti.nl/api/solar/?lastnumber=[ hoeveel laatste waarden je wil]
        public List<Measurement> Get([FromUri] int lastnumber)
        {
           var list = new List<Measurement>();

            try
            {
                _mySqlConnection.Open();
                String sqlstat = "SELECT " + Support.alleVelden + " FROM solardb.Measurement ORDER BY id DESC LIMIT " + lastnumber + ";";
                using (MySqlCommand command = new MySqlCommand(sqlstat, _mySqlConnection))
                {
                    MySqlDataReader reader = command.ExecuteReader();
                    
                    while (reader.Read())
                    {

                        list.Add(new Measurement
                        {
                            Id = reader.GetInt32(0),
                            temperature = reader.GetString(1),
                            longitude = reader.GetString(2),
                            latitude = reader.GetString(3),
                            groundSpeed = reader.GetString(4),
                            course = reader.GetString(5),
                            northsouth = reader.GetString(6),
                            eastwest = reader.GetString(7),
                            foxtimestamp = reader.GetString(8),
                            ipadress = reader.GetString(9),
                            MPPT2AmpsIn = reader.GetString(10),
                            BMSStateOfCharge = reader.GetString(11),
                            BMSPackAmpHours = reader.GetString(12),
                            MPPT2NoCharge = reader.GetString(13),
                            motorControllerMotorCurrent = reader.GetString(14),
                            BMSHighCellVoltage = reader.GetString(15),
                            MPPT2VoltageIn = reader.GetString(16),
                            BMSHighTemp = reader.GetString(17),
                            MPPT4NoCharge = reader.GetString(18),
                            MPPT4VoltageIn = reader.GetString(19),
                            throttlePosition = reader.GetString(20),
                            BMSSummedVoltage = reader.GetString(21),
                            BMSAvgCellVoltage = reader.GetString(22),
                            motorControllerSetSpeed = reader.GetString(23),
                            throttleVoltage = reader.GetString(24),
                            MPPT3VoltageIn = reader.GetString(25),
                            MPPT4TemperatureFlag = reader.GetString(26),
                            GPSSpeed = reader.GetString(27),
                            MPPT3UnderVoltage = reader.GetString(28),
                            GPSLatitude = reader.GetString(29),
                            MPPT4VoltageFlag = reader.GetString(30),
                            MPPT4UnderVoltage = reader.GetString(31),
                            MPPT3NoCharge = reader.GetString(32),
                            MPPT3TemperatureFlag = reader.GetString(33),
                            GPSSattelites = reader.GetString(34),
                            MPPT1VoltageOut = reader.GetString(35),
                            GPSLongtitude = reader.GetString(36),
                            motorControllerCommandWord = reader.GetString(37),
                            MPPT1VoltageIn = reader.GetString(38),
                            motorControllerMotorTemp = reader.GetString(39),
                            BMSPackCurrent = reader.GetString(40),
                            MPPT3VoltageFlag = reader.GetString(41),
                            MPPT1VoltageFlag = reader.GetString(42),
                            motorControllerBusVoltage = reader.GetString(43),
                            GPSAltitude = reader.GetString(44),
                            MPPT4VoltageOut = reader.GetString(45),
                            MPPT1UnderVoltage = reader.GetString(46),
                            MPPT4AmpsIn = reader.GetString(47),
                            BMSLowCellVoltage = reader.GetString(48),
                            logs = reader.GetString(49),
                            MPPT1AmpsIn = reader.GetString(50),
                            MPPT2UnderVoltage = reader.GetString(51),
                            MPPT1NoCharge = reader.GetString(52),
                            MPPT1TemperatureFlag = reader.GetString(53),
                            motorControllerActualSpeed = reader.GetString(54),
                            MPPT3AmpsIn = reader.GetString(55),
                            MPPT2VoltageOut = reader.GetString(56),
                            MPPT2VoltageFlag = reader.GetString(57),
                            MPPT2TemperatureFlag = reader.GetString(58),
                            throttleCentered = reader.GetString(59),
                            MPPT3VoltageOut = reader.GetString(60),
                            BMSLowTemp = reader.GetString(61),
                            frozen = Support.isFrozen(reader.GetString(8)),
                            speed = reader.GetString(63),
                            timeStamp = DateTime.Now.ToString()



                        });
                    }
                   }
            }

            catch (Exception)
            {
               
            }
            _mySqlConnection.Close();
          return list;

        }

        //http://avansti.nl/api/solar/
        public String Post([FromBody]string json)
        {
            String succes = String.Empty;

            try
            {
                Measurement m = JsonConvert.DeserializeObject<Measurement>(json);

                try
                {
                    DateTime dt = DateTime.Parse(m.foxtimestamp, null, DateTimeStyles.RoundtripKind);
                }

                catch (Exception e)
                {
                    m.foxtimestamp = "geen timestamp";
                }

                _mySqlConnection.Open();
                String query = "insert into measurement (" + Support.bijnaAlles + ") values ('" + m.temperature + "','" + m.longitude + "','" + m.latitude + "','" + m.groundSpeed + "','" + m.course + "','" + m.northsouth + "','" + m.eastwest + "','" + m.foxtimestamp + "','" + m.ipadress + "','" + m.MPPT2AmpsIn + "','" + m.BMSStateOfCharge + "','" + m.BMSPackAmpHours + "','" + m.MPPT2NoCharge + "','" + m.motorControllerMotorCurrent + "','" + m.BMSHighCellVoltage + "','" + m.MPPT2VoltageIn + "','" + m.BMSHighTemp + "','" + m.MPPT4NoCharge + "','" + m.MPPT4VoltageIn + "','" + m.throttlePosition + "','" + m.BMSSummedVoltage + "','" + m.BMSAvgCellVoltage + "','" + m.motorControllerSetSpeed + "','" + m.throttleVoltage + "','" + m.MPPT3VoltageIn + "','" + m.MPPT4TemperatureFlag + "','" + m.GPSSpeed + "','" + m.MPPT3UnderVoltage + "','" + m.GPSLatitude + "','" + m.MPPT4VoltageFlag + "','" + m.MPPT4UnderVoltage + "','" + m.MPPT3NoCharge + "','" + m.MPPT3TemperatureFlag + "','" + m.GPSSattelites + "','" + m.MPPT1VoltageOut + "','" + m.GPSLongtitude + "','" + m.motorControllerCommandWord + "','" + m.MPPT1VoltageIn + "','" + m.motorControllerMotorTemp + "','" + m.BMSPackCurrent + "','" + m.MPPT3VoltageFlag + "','" + m.MPPT1VoltageFlag + "','" + m.motorControllerBusVoltage + "','" + m.GPSAltitude + "','" + m.MPPT4VoltageOut + "','" + m.MPPT1UnderVoltage + "','" + m.MPPT4AmpsIn + "','" + m.BMSLowCellVoltage + "','" + m.logs + "','" + m.MPPT1AmpsIn + "','" + m.MPPT2UnderVoltage + "','" + m.MPPT1NoCharge + "','" + m.MPPT1TemperatureFlag + "','" + m.motorControllerActualSpeed + "','" + m.MPPT3AmpsIn + "','" + m.MPPT2VoltageOut + "','" + m.MPPT2VoltageFlag + "','" + m.MPPT2TemperatureFlag + "','" + m.throttleCentered + "','" + m.MPPT3VoltageOut + "','" + m.BMSLowTemp + "','" + m.frozen + "','" + m.speed + "','" + DateTime.Now.ToLocalTime() + "');";
                _cmd = new MySqlCommand(query, _mySqlConnection);
                _cmd.ExecuteNonQuery();
                succes = "succes";
            }

            catch (Exception e)
            {
                succes = e.Message;
            }
            _mySqlConnection.Close();
            return succes;
        }


    }
}
